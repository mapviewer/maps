export interface Zone {
  _id?: string;
  code: string;
  nombre: string;
  location: {
    type: 'Polygon',
    coordinates: number[][][];
  }
  userId?: string;
  updatedAt?: Date;
  createdAt?: Date;
  // @TODO Example
  // type: "Polygon",
  // coordinates: [[[0, 0], [3, 6], [6, 1], [0, 0]]]
}
