import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { MeteorObservable, ObservableCursor } from 'meteor-rxjs';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';

import { Zone, Register } from 'imports/models';
import { Zones, Registers } from 'imports/collections';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'register-chart',
  templateUrl: 'register-chart.html',
})
export class RegisterChartComponent implements OnInit, OnDestroy {
  options: Object;
  basicSeries = {
    co: {
      name: 'Monoxido de carbono CO',
      id: 'CO',
      data: [],
    },
    co2: {
      name: 'Dioxido de carbono CO2',
      id: 'CO2',
      data: [],
    },
    no2: {
      name: 'Dioxido de nitrogeno NO2',
      id: 'NO2',
      data: [],
    },
    so2: {
      name: 'Dioxido de azufre SO2',
      id: 'SO2',
      data: [],
    },
    pm10: {
      name: 'Particulas suspendidas PM10',
      id: 'PM10',
      data: [],
    },
  };
  orderSeries = ['co', 'co2', 'no2', 'so2', 'pm10'];
  zones: ObservableCursor<Zone>;
  currentZone: Zone;
  counts: any;
  dateFormat = 'DD/MM/YY HH:mm';

  private lastZoneCode: string;
  private registerSubs: Subscription;
  private autorunSubs: Subscription;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor() {}

  ngOnInit() {
    this.options = this.buildBasicOptions();
    // @TODO Subcribing to all zones, si hay inserciones de zonas, tendre los cambios directamente
    MeteorObservable.subscribe('Zones.all').takeUntil(this.ngUnsubscribe).subscribe(() => {
      MeteorObservable.autorun().takeUntil(this.ngUnsubscribe).subscribe(() => {
        this.zones = Zones.find({}, { sort: { code: 1 } });
      });
    });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  subscribeRegisters() {
    // @TODO solo me subscribo si aun no existe o si el codigo de zona es diferente a la ultima subscripcion, para que matar la subscripcion si es que ya estoy correctamente subscrito
    if (!this.currentZone ||
      (this.lastZoneCode && this.lastZoneCode === this.currentZone.code)) {
      return;
    }
    this.unSubscribeRegiters();
    this.lastZoneCode = this.currentZone.code;

    // @TODO guardo la subscripcion para luego matarla, lastimosamente no mata la subscripcion manualmente
    this.registerSubs = MeteorObservable.subscribe('Registers.byZone', this.currentZone._id).takeUntil(this.ngUnsubscribe).subscribe(() => {
      this.autorunSubs = MeteorObservable.autorun().subscribe(() => {
        this.buildRegisters();
      });
    });
  }

  unSubscribeRegiters(updateChart = false) {
    if (this.registerSubs) {
      this.registerSubs.unsubscribe();
      this.autorunSubs.unsubscribe();
    }
    if (updateChart) {
      this.lastZoneCode = undefined;
      this.options = this.buildBasicOptions();
    }
  }

  buildRegisters(): void {
    const options = this.buildBasicOptions(this.currentZone.code);
    const registers = Registers.find({ zoneId: this.currentZone._id }, { sort: { date: 1 } }).fetch();

    const data = { co: [], co2: [], no2: [], so2: [], pm10: [] };
    registers.forEach((register: Register) => {
      const year = register.date.getUTCFullYear();
      const month = register.date.getUTCMonth();
      const day = register.date.getUTCDate();
      const dateUTC = Date.UTC(year, month, day);
      
      this.orderSeries.forEach(key => {
        const value = register[key];
        if (Number.isInteger(value)){
          data[key].push([dateUTC, value])
        }
      })
    });
    options.series = this.orderSeries.map(key => {
      const { name, id } = this.basicSeries[key];
      return { name, id, data: data[key] };
    });

    this.options = options;
  }

  private buildBasicOptions(code = ''): any {
    // @TODO Review: https://api.highcharts.com/highcharts/
    const dateFormat = this.dateFormat;
    const options = {
      chart: {
        type: 'spline'
      },
      title: {},
      xAxis: {
        type: 'datetime',
        dateTimeLabelFormats: {
          day: '%d\/%m\/%y',
          week: '%d\/%m\/%y',
          month: '%m\/%y',
          year: '%y',
        },
      },
      yAxis: {
        title: {
          text: 'Nivel contaminacion'
        },
        min: 0
      },
      tooltip: {
        headerFormat: '<b>{point.x:%d\/%m\/%y}</b>: ',
        formatter(opts) {
          const [firstPoint] = this.points;
          const texts = [`<b style="font-weight:bold; font-size:14px">${moment(firstPoint.x).format(dateFormat)}</b>`];
          this.points.forEach(point => {
            texts.push(`<span style="color:${point.color}; font-weight:bold;">${point.series.options.id}:</span> ${point.y}`);
          });
          return texts.join('<br/>');
        },
        shared: true,
        useHTML: true,
      },

      plotOptions: {
        spline: {
          marker: {
            enabled: true
          }
        }
      },

      series: this.orderSeries.map(key => this.basicSeries[key]),
    };
    if (code) {
      options.title = {
        text: `Showing contamination of ${code}`,
      };
    }
    return options;
  }

}