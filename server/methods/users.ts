// @TODO No encontre solucion aun para fixear este problema de typing
import { Roles } from 'meteor/alanning:roles';

import { User } from '../../imports/models';

Meteor.methods({
  'Users.create'(opts: any): string {
    const  { username, email, password, profile, isAdmin } = opts;
    const options = { username, email, password, profile };
    const userId = Accounts.createUser(opts);
   
    if (isAdmin) {
      Roles.addUsersToRoles(userId, ['admin', 'viewer'], Roles.GLOBAL_GROUP);
    } else {
      Roles.addUsersToRoles(userId, ['viewer'], Roles.GLOBAL_GROUP);
    }
    return userId;
  },
});
