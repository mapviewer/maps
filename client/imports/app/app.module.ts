import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AgmCoreModule } from '@agm/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToasterModule } from 'angular2-toaster';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MomentModule } from 'angular2-moment';
import { ChartModule } from 'angular2-highcharts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapViewerComponent, PageNotFoundComponent, ZoneFormComponent, LoginComponent, ZoneFileComponent, RegisterChartComponent, UnauthorizedComponent } from './components';
import { UserService } from './services';

declare const Roles: any;

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAWoBdZHCNh5R-hB5S5ZZ2oeoYyfdDgniA'
    }),
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    ToasterModule,
    MomentModule,
    ChartModule.forRoot(require('highcharts')),
  ],
  declarations: [
    AppComponent,
    MapViewerComponent,
    ZoneFormComponent,
    PageNotFoundComponent,
    LoginComponent,
    ZoneFileComponent,
    RegisterChartComponent,
    UnauthorizedComponent,
  ],
  providers: [
    UserService,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }
