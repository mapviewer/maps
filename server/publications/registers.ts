import { Meteor } from 'meteor/meteor';

import { Registers } from '../../imports/collections';

Meteor.publish('Registers.byZone', (zoneId: string) => {
  if (!Meteor.userId()) {
    // User not logged doesn't return
    return;
  }
  return Registers.find({ zoneId }, { sort: { date: 1 }});
});

// Meteor.publish('Users.byUserId', (userIds: string[]) => {
//   if (!Meteor.userId()) {
//     // User not logged doesn't return
//     return;
//   }
//   // db.inventory.find( { status: { $in: [ "A", "D" ] } } )

//   return Meteor.users.find({ _id: { $in: userIds } });
// });

