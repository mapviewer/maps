import { Meteor } from 'meteor/meteor';
import * as moment from 'moment';
import { MeteorObservable, ObservableCursor } from 'meteor-rxjs';
import { MapPoint, MapPointInfo, Zone } from '../../imports/models';
import { Register } from '../../imports/models';
import { Registers, Zones } from '../../imports/collections';
import { ToasterService } from 'angular2-toaster';

const dateFormat = 'DD/MM/YYYY HH:mm';

Meteor.methods({
  
  'CSV.process'(lines: string[]): number {
    if (!Meteor.userId()) {
      throw new Meteor.Error(401, 'User not logged, method not allowed');
    }
    let processed = 0;
    lines.forEach(line => {
      try {
        if(processed>0) {
        processLine(line);        
        }
        processed++;
      } catch (error) {
        console.log(error);
      }
    });
    return processed;
  }
});

function processLine(line: string) {
  console.log(line, '################################');
  let [code, date, co, co2, no2, so2, pm10, zona] = line.split(';');
  var cor, co2r, no2r,so2r,pm10r;
  
  const [,finalDate] = date.split('\''); 

  console.log(finalDate, moment(finalDate, dateFormat, true).isValid());
  var mDate = moment(finalDate, dateFormat)  ; // esto es lo correcto para crear date desde strings
  var rdate = mDate.toDate() ;  
  let zoneSaved: Zone = Zones.collection.findOne({ code});
  if (!zoneSaved){
 
    console.log(JSON.stringify({code, date, co, co2, no2, so2, pm10, zona}));

    
    const condenadas=zona.split(" ").map((item)=>{ 
      const [lat,log]= item.split("|")
      return [Number(log),Number(lat)];
      
    });
    condenadas.push(condenadas[0]);

    let zone: any = { code:code};
    zone.userId = Meteor.userId();
    zone.createdAt = new Date();  
    zone.location={ type: 'Polygon', coordinates: [condenadas] };  
    zoneSaved = Meteor.call('Zones.insert', zone);
  }  


  //console.log('processLine', 'zone', zone);
  if (co){ cor =Number(co);} else {  cor=null;}
  if (co2!=null){ co2r =Number(co2);} else {  co2r=null;}
  if (no2!=null){ no2r =Number(no2);} else {  no2r=null;}
  if (so2 == undefined ||so2==null){ so2=null; } else { so2r =Number(so2); }
  if (pm10){ pm10r =Number(pm10);} else { pm10r=null;}
  let register: any = {date:rdate, zoneId: zoneSaved._id ,co:cor,co2:co2r,no2:no2r,so2:so2r,pm10:pm10r};

  register = Meteor.call('Registers.insert', register);

}

