import { MapPoint, Register } from './index';

export interface MapPointInfo {
  point: MapPoint;
  zone: {
    code: string;
    nombre: string;
  };
  register?: Register;
}
