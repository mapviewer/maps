import { Meteor } from 'meteor/meteor';

import { Zones } from '../../imports/collections';

Meteor.publish('Zones.byId', (_id: string) => {
  if (!Meteor.userId()) {
    return;
  }
  return Zones.find({ _id });
});

Meteor.publish('Zones.all', (_id: string) => {
  if (!Meteor.userId()) {
    return;
  }
  return Zones.find({});
});
