import { Meteor } from 'meteor/meteor';

import { Register } from '../../imports/models';
import { Registers, Zones } from '../../imports/collections';

Meteor.methods({
  'Registers.insert'(register: Register): Register {
    if (!Meteor.userId()) {
      throw new Meteor.Error(401, 'User not logged, method not allowed');
    }
    if (!Zones.findOne({ _id: register.zoneId })) {
      throw new Meteor.Error(404,'Zone doesn\'t exist');
    }
    console.log(register);
    ['co', 'co2', 'no2', 'so2', 'pm10'].forEach(key => {
      if (register[key] !== 0 && !register[key]) {
        register[key] = null;
      }
    });
    console.log(register);
    register.userId = Meteor.userId();
    register.createdAt = new Date();
    register._id = Registers.collection.insert(register);
    return register;
  },
  
});
