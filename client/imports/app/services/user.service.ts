import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MeteorObservable } from 'meteor-rxjs';
import { Roles } from 'meteor/alanning:roles';

import { User } from 'imports/models';

export type LoggedState = 'LOGGED' | 'UNLOGGED' | 'LOGGING';
@Injectable()
export class UserService {
  private state: LoggedState = 'LOGGING';
  private hasRoles = {
    isAdmin: true,
    isViewer: true,
  }
  private user: User;

  constructor() {
    // @TODO esta accion esta viva mientras dure la aplicacion.
    MeteorObservable.autorun()
      .subscribe(() => {
        this.user = Meteor.user() as User;
        if (Meteor.loggingIn()) {
          this.state = 'LOGGING';
        } else {
          this.state = this.user ? 'LOGGED' : 'UNLOGGED';
        }
        this.loadRole(this.user);
      });
  }

  getState(): LoggedState {
    return this.state;
  }

  getUser(): User {
    return this.user;
  }

  isLogged(): boolean {
    return this.state === 'LOGGING' || this.state === 'LOGGED';
  }

  isAdmin(): boolean {
    return this.state === 'LOGGED' && this.hasRoles.isAdmin;
  }

  isViewer(): boolean {
    return this.state === 'LOGGED' && this.hasRoles.isViewer;
  }

  private loadRole(user: User): void {
    if (!user) {
      this.hasRoles = { isAdmin: false, isViewer: false };
      return;
    }
    this.hasRoles = {
      isAdmin: Roles.userIsInRole(Meteor.user(), ['admin']),
      isViewer: Roles.userIsInRole(Meteor.user(), ['viewer']),
    }
  }
}