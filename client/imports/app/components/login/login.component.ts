import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterService, Toast } from 'angular2-toaster';
import { MeteorObservable } from 'meteor-rxjs';

@Component({
  selector: 'login',
  templateUrl: 'login.html',
})
export class LoginComponent implements OnInit {
  typeForm: 'LOGIN' | 'SIGNUP' = 'LOGIN';

  username: string;
  password: string;
  email: string;
  isAdmin: boolean=false;
  
  constructor(
    private toasterService: ToasterService,
    private router: Router,
    private ngZone: NgZone
  ) { }

  ngOnInit() {
    
    
  }

  login(): void {
    
    console.log('login', { username: this.username, password: this.password });
  
    Meteor.loginWithPassword(this.username, this.password, (error, result) => {
      
      // https://blog.thoughtram.io/angular/2016/02/01/zones-in-angular-2.html
      this.ngZone.run(() => {
        if (error) {
          this.toasterService.pop('error', 'LOGIN', error.reason);
        } else {
          this.toasterService.pop('success', 'LOGIN', 'HA SIDO LOGUEADO');
          this.router.navigateByUrl('/map');
        }
      })
    });
  }

  signup(): void {
    const options = {
      username: this.username,
      email: this.email,
      password: this.password,
      profile: {},
      isAdmin: this.isAdmin,
    };

    MeteorObservable.call('Users.create', options).subscribe(
      (zone: Zone) => {
        Meteor.loginWithPassword(this.username, this.password, (error) => {
          
          this.toasterService.pop('success', 'SIGNUP', 'HA SIDO CREADO');
          this.router.navigateByUrl('/map');
        });
      }, (error) => {
        console.log(error);
        this.toasterService.pop('error', 'SIGNUP', error.reason);
      }
    )
    console.log('signup', options);
  }
}
