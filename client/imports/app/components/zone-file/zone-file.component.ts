import { Component, OnInit } from '@angular/core';
import { MeteorObservable } from 'meteor-rxjs';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'zone-file',
  templateUrl: 'zone-file.html',
})
export class ZoneFileComponent implements OnInit {
  constructor(
    private toasterService: ToasterService,
  ) {}
  ngOnInit() {}

  onChange(evt) {
    const file = evt.dataTransfer ? evt.dataTransfer.files[0] : evt.target.files[0];
    if (file.type !== 'text/csv') {
      this.toasterService.pop('error', 'Import CSV', 'Invalid file. Please selecte a CSV file.');
      return;
    }
    const reader = new FileReader();
    reader.onload = () => {
      this.saveFile(reader.result);
    };
    reader.onerror = (...args) => {
      this.toasterService.pop('error', 'Import CSV', 'Error while loading csv file, please try again');
    }
    reader.readAsText(file);
  }

  private saveFile(allString: string): void {
    let lines = allString.split('\n');
    // @TODO Mantener solo las lineas con datos
    lines = lines.filter(item => !!item);
    MeteorObservable.call('CSV.process', lines).subscribe(
      (linesProcessed: number) => {
        this.toasterService.pop('success', 'Import CSV', `"${linesProcessed}" lines processed sucessfully`);
      }, (error) => {
        this.toasterService.pop('error', 'Import CSV', 'Error processing file');
      }
    );
  }
}
