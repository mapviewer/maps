export interface Register {
  _id?: string;
  zoneId: string;
  date: Date;
  // null means no information
  co?: number;
  co2?: number;
  no2?: number;
  so2?: number;
  pm10?: number;

  userId?: string;
  updatedAt?: Date;
  createdAt?: Date;
}
