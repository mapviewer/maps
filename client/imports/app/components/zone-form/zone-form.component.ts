import { Component, OnInit, OnDestroy } from '@angular/core';
import { LatLng, LatLngLiteral } from '@agm/core';
import { MeteorObservable, ObservableCursor } from 'meteor-rxjs';
import { Subject } from 'rxjs/Subject';
import * as moment from 'moment';
import { ToasterService } from 'angular2-toaster';

import { Zone, Register } from '../../../../../imports/models';
import { Registers } from '../../../../../imports/collections';

@Component({
  selector: 'zone-form',
  templateUrl: 'zone-form.html',
})
export class ZoneFormComponent implements OnInit, OnDestroy {
  step: 'ZONE' | 'REGISTER';
  currentZone: Zone;
  zoneMode: 'CREATION' | 'UPDATE' | 'SELECTION' | 'NONE' = 'NONE';

  iniLat = -17.378204128263384;
  iniLng = -66.16138458251953;
  zoom = 13;
  paths: Array<LatLngLiteral> = [];

  registers: ObservableCursor<Register>;
  currentReg: Register;
  regDateStr: string;
  dateFormat = 'DD/MM/YYYY HH:mm';

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private toasterService: ToasterService,
  ) {}

  ngOnInit() {
    this.step = 'ZONE';
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  subscribeZone() {
    if (this.currentZone && this.currentZone._id && this.step === 'REGISTER') {
      // @TODO https://medium.com/@benlesh/rxjs-dont-unsubscribe-6753ed4fda87
      MeteorObservable.subscribe('Registers.byZone', this.currentZone._id).takeUntil(this.ngUnsubscribe).subscribe(() => {
        MeteorObservable.autorun().takeUntil(this.ngUnsubscribe).subscribe(() => {
          this.registers = Registers.find({ zoneId: this.currentZone._id }, { sort: { date: -1 } });
          if (!this.currentReg) {
            this.currentReg = Registers.findOne({}, { sort: { date: -1 } });
            this.buildDateRegister();
          }
        });
      });
    }
    // MeteorObservable.autorun().takeUntil(this.ngUnsubscribe).subscribe(() => {
    //   const registros = Registers.find({ zoneId: this.currentZone._id }, { sort: { date: -1 } }).toArray();
    //   const userIds = [] // registros;
    //   MeteorObservable.subscribe('Users.byUserId', userIds).takeUntil(this.ngUnsubscribe).subscribe(() => {
    //   angUsers: {
     
        //  us2: 'NOEMI
    //   }

    //   });
    // });
  }

  // getUser(userId: string) {
  //   return angUsers[userId]
  // }

  onMapClick({ coords }: any): void {
    if (this.paths.length === 0) {
      this.paths = [coords, coords];
    } else {
      this.paths.splice(this.paths.length - 1, 0, coords);
      // @TODO Inmutabilidad, recreamos el array para que angular lo vuelva a dibujar, sin eso angular no actualizara el area
      this.paths = [...this.paths];
    }
  }

  cargarZona(code: string) {
    // Esta accion hace que el formulario se ponga en modo seleccion o creacion
    MeteorObservable.call('Zones.getByCode', code).subscribe(
      (zone: Zone) => {
        console.log(zone);
        this.currentZone = zone;
        this.paths = this.buildPath(this.currentZone.location.coordinates);
        this.zoneMode = 'SELECTION';
      }, (error) => {
        console.log(error);
        this.zoneMode = 'CREATION';
        this.currentZone = {
          code: code,
          nombre: '',
          location: { type: 'Polygon', coordinates: [] },
        };
        this.paths = [];
      }
    )
  }

  resetPoligon(): void {
    this.paths = [];
    if (this.zoneMode === 'SELECTION') {
      this.zoneMode = 'UPDATE';
    }
  }

  selectZone(): void {
    this.step = 'REGISTER'
    this.subscribeZone();
  }

  createZone(): void {
    this.currentZone.location.coordinates = this.buildCoordinates(this.paths);
    MeteorObservable.call('Zones.insert', this.currentZone).subscribe(
      (zone: Zone) => {
        this.currentZone = zone;
        this.step = 'REGISTER';
        this.subscribeZone();
      }, (error) => {
        console.log(error);
      }
    )
  }

  updateZone(): void {
    // Implementar la actualizacion en zona
    this.currentZone.location.coordinates = this.buildCoordinates(this.paths);
    this.step = 'REGISTER';
    this.zoneMode = 'SELECTION';
  }

  buildPath(coordinates: number[][][]): Array<LatLngLiteral> {
    const result = coordinates[0].map((coord) => {
      const [lng, lat] = coord;
      return { lng, lat };
    });
    return result;
  }

  buildCoordinates(paths: Array<LatLngLiteral>): number[][][] {
    // @TODO Ecma6 http://es6-features.org/#ObjectMatchingShorthandNotation
    const points = paths.map(({ lat, lng }) => {
      return [lng, lat];
    });
    return [points];
  }

  // REGISTERS
  isSelected(register: Register): boolean {
    if (!this.currentReg) {
      return false;
    }
    return this.currentReg.date.getTime() === register.date.getTime();
  }

  startRegister(): void {
    this.currentReg = {
      date: new Date(),
      zoneId: this.currentZone._id,
    } as Register;

    this.buildDateRegister();
  }

  buildDateRegister(): void {
    if (this.currentReg && this.currentReg.date) {
      this.regDateStr = moment(this.currentReg.date).format(this.dateFormat);
    }
  }

  createRegister(): void {
    // @TODO https://momentjs.com/docs/#/parsing/string-formats/
    if (this.regDateStr === '' || !moment(this.regDateStr, this.dateFormat, true).isValid()) {
      this.toasterService.pop('error', 'Register', 'Date format is invalid');
      return;
    }
    if (!this.hasAValue(this.currentReg)) {
      this.toasterService.pop('error', 'Register', 'It is require at least one value');
      return;
    }
    this.currentReg.date = moment(this.regDateStr, this.dateFormat).toDate();
    MeteorObservable.call('Registers.insert', this.currentReg).subscribe(
      (regCreated: Register) => {
        this.currentReg = regCreated;
        this.toasterService.pop('success', 'Register', 'Register successfully created');
      }, (error) => {
        this.toasterService.pop('error', 'Register', error.message);
      }
    )
  }

  private hasAValue(register: Register): boolean {
    const { co, co2, no2, so2, pm10 } = register;
    return co > 0 || co2 > 0 || no2 > 0 || so2 > 0 || pm10 > 0;
  }

}
