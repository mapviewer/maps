import { Component } from '@angular/core';

@Component({
  selector: 'unauthorized',
  templateUrl: 'unauthorized.html'
})
export class UnauthorizedComponent { }
