import { MongoObservable } from 'meteor-rxjs';

import { Zone } from '../models';

export const Zones = new MongoObservable.Collection<Zone>('zones');
