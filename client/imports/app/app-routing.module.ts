import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MapViewerComponent, PageNotFoundComponent, ZoneFormComponent, LoginComponent, ZoneFileComponent, RegisterChartComponent, UnauthorizedComponent } from './components';
import { ViewerGuard, AdminGuard } from './services';

const appRoutes = [{
  path: 'login',
  component: LoginComponent,
}, {
  path: 'map',
  component: MapViewerComponent,
}, {
  path: 'zone',
  component: ZoneFormComponent,
  canActivate: [ViewerGuard],
}, {
  path: 'import',
  component: ZoneFileComponent,
  canActivate: [ViewerGuard],
}, {
  path: 'chart',
  component: RegisterChartComponent,
  canActivate: [AdminGuard],
}, {
  // 403 Forbidden
  path: 'unauthorized',
  component: UnauthorizedComponent,
}, {
  // Home Page
  path: '',
  redirectTo: '/map',
  pathMatch: 'full'
}, {
  // 404 Page
  path: '**',
  component: PageNotFoundComponent,
}];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes),
  ],
  providers: [
    AdminGuard,
    ViewerGuard,
  ],
  exports: [
    RouterModule,
  ]
})

export class AppRoutingModule { }
