import { Component } from '@angular/core';
import { MeteorObservable } from 'meteor-rxjs';
import { ToasterService } from 'angular2-toaster';

import { MapPoint, MapPointInfo, Register } from '../../../../../imports/models';

@Component({
  selector: 'map-viewer',
  templateUrl: 'map-viewer.html',
})
export class MapViewerComponent {
  iniLat = -17.378204128263384;
  iniLng = -66.16138458251953;
  zoom = 15;

  info: MapPointInfo;
  constructor(
    private toaster: ToasterService,
  ) {}

  onMapClick({ coords }: any) {
    this.info = undefined;
    const { lat: latitude, lng: longitude } = coords;
    MeteorObservable.call('Query.contamination', { latitude, longitude } ).zone().subscribe(
      (response: MapPointInfo) => {
        if (!response) {
          this.toaster.pop('warning', 'Viewer', 'No existe zona registrada en la ubicacion.');
        } else if (!response.register) {
          this.toaster.pop('warning', 'Viewer', `No hay registro en la zona "${response.zone.code}".`);
        } else {
          this.info = response;
        }
        console.log('Response in client', this.info)
      }, (error) => {
        this.toaster.pop('success', 'Viewer', error.message);
      }
    );
  }

  parseValue(key: string): string {
    const register: Register = this.info.register;
    const value = register[key];
    if (!value && value !== 0) {
      return '--';
    }
    return `${value}`;
  }
}
