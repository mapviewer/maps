import { Meteor } from 'meteor/meteor';

import { MapPoint, MapPointInfo, Zone, Register } from '../../imports/models';
import { Zones, Registers } from '../../imports/collections';

Meteor.methods({
  'Query.contamination'(point: MapPoint): MapPointInfo {
    const coordinates = [point.longitude, point.latitude];
    const zone = Zones.collection.findOne({ location: {
      $geoIntersects: {
        $geometry: { type: 'Point', coordinates }
      }
    }});
    if (!zone) {
      return null;
    }
    const { code, nombre } =zone;
    const result: MapPointInfo = { point, zone: { code, nombre } };
    const lastReg = Registers.collection.findOne({ zoneId: zone._id }, { sort: { date: 1 }});
    if (!lastReg) {
      return result;
    }
    result.register = lastReg;
    return result;
  }
});
