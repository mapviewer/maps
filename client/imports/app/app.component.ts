import { Meteor } from 'meteor/meteor';
import { MeteorObservable } from 'meteor-rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs/Subject';

import { User } from '../../../imports/models';
import { UserService } from './services';

@Component({
  selector: 'app',
  templateUrl: 'app.html',
})
export class AppComponent implements OnInit {
  constructor(
    private userService: UserService,
  ) { }

  ngOnInit() { }

  isLogged(): boolean {
    return this.userService.isLogged();
  }

  isAdmin(): boolean {
    return this.userService.isAdmin();
  }

  isViewer(): boolean {
    return this.userService.isViewer();
  }

  public get user(): User {
    return this.userService.getUser();
  }

  logout(): void {
    Meteor.logout((...args) => {
      console.log('User is logout');
    })
  }
}
