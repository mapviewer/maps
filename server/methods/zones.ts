import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';

import { Zone, Register } from '../../imports/models';
import { Zones, Registers } from '../../imports/collections';

Meteor.methods({
  'Zones.insert'(zone: Zone): Zone {
    if (!Meteor.userId()) {
      throw new Meteor.Error(401, 'User not logged, method not allowed');
    }
    if (Zones.findOne({ code: zone.code })) {
      throw new Meteor.Error(401, 'Code already exist, use another code');
    }
    zone.userId = Meteor.userId();
    zone.createdAt = new Date();
    console.log(zone);    
    zone._id = Zones.collection.insert(zone);
    return zone;
  },

  'Zones.getByCode'(code: string): Zone {  
    
    if (!Meteor.userId()) {
      throw new Meteor.Error(401, 'User not logged, method not allowed');
    }  
    const zone = Zones.collection.findOne({ code: code });
    if (!zone) {
      throw new Meteor.Error(401, `Zone with code "${code}" does not exist`);
    }
    return zone;
  },

 
});
